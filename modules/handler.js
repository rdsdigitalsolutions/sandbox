"use strict";

const auth = require("pg-auth-api");

module.exports.hello = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: "Go Serverless v2.0! Your function executed successfully!",
        input: event,
      },
      null,
      2
    ),
  };
};

module.exports.auth = async (event) => {
  return JSON.stringify(await auth.authenticate(event));
};
