# Sandbox


## Adding modules

1. Include a handler in this file:

        modules/handler.js

2. Then import your handler packager (for example):

        "dependencies": {
            "pg-auth-api":"^1.0.2"
        }

> Make sure you publish the module and that it's versioned correctly.

- https://www.npmjs.com/package/pg-auth-api
- https://bitbucket.org/rdsdigitalsolutions/pg-auth-api


## Start The Lambda server on localhost.

    npm run build && npm run start

## Start testing your modules via postman (import this curl):

    curl --location --request POST 'http://localhost:3000/dev/auth' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "login": "rafael",
        "pass": 123
    }'


